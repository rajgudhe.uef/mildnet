import torch
import torch.nn as nn
import torch.nn.functional as F

class MLDRBlock(nn.Module):
    def __init__(self, in_channels):
        super(MLDRBlock, self).__init__()
        
        self.conv1_1 = nn.Conv2d(in_channels, in_channels, kernel_size=3, padding=1)
        self.bn1_1 = nn.BatchNorm2d(in_channels)
        
        self.conv1_2 = nn.Conv2d(in_channels, in_channels, kernel_size=3, padding=1)
        self.bn1_2 = nn.BatchNorm2d(in_channels)
        
        self.conv2_1 = nn.Conv2d(in_channels, in_channels, kernel_size=3, padding=3, dilation=3)
        self.bn2_1 = nn.BatchNorm2d(in_channels)
        
        self.conv2_2 = nn.Conv2d(in_channels, in_channels, kernel_size=3, padding=3, dilation=3)
        self.bn2_2 = nn.BatchNorm2d(in_channels)
        
        self.conv3_1 = nn.Conv2d(in_channels, in_channels, kernel_size=3, padding=5, dilation=5)
        self.bn3_1 = nn.BatchNorm2d(in_channels)
        
        self.conv3_2 = nn.Conv2d(in_channels, in_channels, kernel_size=3, padding=5, dilation=5)
        self.bn3_2 = nn.BatchNorm2d(in_channels)
        
        self.conv_skip = nn.Conv2d(in_channels, in_channels, kernel_size=1)
        self.bn_skip = nn.BatchNorm2d(in_channels)
        
    def forward(self, x):
        identity = x
        
        out = F.relu(self.bn1_1(self.conv1_1(x)))
        out = self.bn1_2(self.conv1_2(out))
        
        out = F.relu(self.bn2_1(self.conv2_1(out)))
        out = self.bn2_2(self.conv2_2(out))
        
        out = F.relu(self.bn3_1(self.conv3_1(out)))
        out = self.bn3_2(self.conv3_2(out))
        
        out = out + self.bn_skip(self.conv_skip(identity))
        out = F.relu(out)
        
        return out

class MLRBlock(nn.Module):
    def __init__(self, in_channels):
        super(MLRBlock, self).__init__()
        
        self.mldr1 = MLDRBlock(in_channels)
        self.mldr2 = MLDRBlock(in_channels)
        
    def forward(self, x):
        out = self.mldr1(x)
        out = self.mldr2(out + x)
        return out

class MILDNet(nn.Module):
    def __init__(self, in_channels=3, out_channels=1):
        super().__init__()
        
        # Encoder
        self.conv1 = MLDRBlock(in_channels, 64, d_rates=[1, 3, 5])
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv2 = MLDRBlock(64, 128, d_rates=[1, 3, 5])
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv3 = MLDRBlock(128, 256, d_rates=[1, 3, 5])
        self.pool3 = nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv4 = MLDRBlock(256, 512, d_rates=[1, 3, 5])
        self.pool4 = nn.MaxPool2d(kernel_size=2, stride=2)
        
        # Decoder
        self.upconv4 = nn.ConvTranspose2d(512, 256, kernel_size=2, stride=2)
        self.conv5 = MLRBlock(512, 256)
        self.upconv3 = nn.ConvTranspose2d(256, 128, kernel_size=2, stride=2)
        self.conv6 = MLRBlock(256, 128)
        self.upconv2 = nn.ConvTranspose2d(128, 64, kernel_size=2, stride=2)
        self.conv7 = MLRBlock(128, 64)
        self.upconv1 = nn.ConvTranspose2d(64, 32, kernel_size=2, stride=2)
        self.conv8 = nn.Conv2d(32, out_channels, kernel_size=1)
        
    def forward(self, x):
        # Encoder
        x1 = self.conv1(x)
        x = self.pool1(x1)
        x2 = self.conv2(x)
        x = self.pool2(x2)
        x3 = self.conv3(x)
        x = self.pool3(x3)
        x4 = self.conv4(x)
        x = self.pool4(x4)
        
        # Decoder
        x = self.upconv4(x)
        x = torch.cat([x, x3], dim=1)
        x = self.conv5(x)
        x = self.upconv3(x)
        x = torch.cat([x, x2], dim=1)
        x = self.conv6(x)
        x = self.upconv2(x)
        x = torch.cat([x, x1], dim=1)
        x = self.conv7(x)
        x = self.upconv1(x)
        x = self.conv8(x)
        
        return x
    
