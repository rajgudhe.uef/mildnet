import os
from PIL import Image
from torch.utils.data import Dataset
from sklearn.model_selection import train_test_split

class ISICDataset(Dataset):
    def __init__(self, data_dir, train=True, transform=None):
        self.transform = transform
        self.train = train
        self.data_dir = data_dir
        self.images_dir = os.path.join(data_dir, "Images")
        self.masks_dir = os.path.join(data_dir, "Masks")
        
        self.image_names = sorted(os.listdir(self.images_dir))
        self.mask_names = sorted(os.listdir(self.masks_dir))
        
        self.train_image_names, self.val_image_names, _, _ = train_test_split(
            self.image_names, self.mask_names, 
            test_size=0.3, random_state=42, shuffle=True
        )
        
        if self.train:
            self.image_names = self.train_image_names
        else:
            self.image_names = self.val_image_names
            
    def __len__(self):
        return len(self.image_names)
    
    def __getitem__(self, idx):
        image_path = os.path.join(self.images_dir, self.image_names[idx])
        mask_path = os.path.join(self.masks_dir, self.mask_names[idx])
        
        image = Image.open(image_path).convert('RGB')
        mask = Image.open(mask_path).convert('L')
        
        if self.transform is not None:
            image = self.transform(image)
            
        return image, mask
