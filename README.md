**Multi-level dilated residual network for biomedical image segmentation**


We will make our code public here upon publication of our manuscript "Multi-level dilated residual network for biomedical image segmentation". 




The main objective of this work is to implement an end-to-end pipelibe for biomedical image segmentation task using deep learning. We incorporated dilated convolutions with a multi residual connection for the convolutional blocks of the classical U-Net architecture. 

<p align="center">
 <img src="./images/main.png" alt="Drawing" width="80%">
</p>


***Datsets***

<p align="center">
 <img src="./images/datasets.png" alt="Drawing" width="80%">
</p>


****ISIC-2018 (Dermoscopy)****
Dermoscopy is an imaging technique thateliminates the skin surface reflection to en-hance visualization of the deeper skin lay-ers. We have acquired the dermoscopy im-ages from the ISIC-2018; skin lesion analy-sis towards melanoma detection challenge

****ISBI-2012 (Electron microsopy)****
This   dataset   contains   a   serial   sectiontransmission  electron  microscopy  of  thedrosophila first instar larva ventral nervecord. The dataset is provided by the ISBI-2012;  2D electron microscopy segmenta-tion challenge.

****MRI (Magnetic resonance imaging)*****
This  dataset  contains  brain  MRI  imagesand segmentation masks created by man-ual fluid-attenuated inversion recovery ac-quired from 110 patients included in thecancer genome atlas lower-grade collection.

****Glas-2015 (Histopathology)****
This dataset acquired from the gland seg-mentation in colon histology image chal-lenge. The images are scanned whole-slidehistology images of the colon, in which ep-ithelial glands are annotated.

****DSB-2018 (Cell nuceli microscopy)****
This dataset contains segmented nuclei im-ages acquired under different conditions bychanging the cell type, magnification, andimaging modality (bright-field vs. fluores-cence).

The struture of the data folder is as follows:

```bash
├── data
	├── ISIC
	│   ├── images
	│   ├── masks
    ├── MRI
	    ├── images
	    ├── masks

```


